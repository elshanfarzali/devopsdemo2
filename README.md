# DevOps Project Demo 2
## Deploying Spring PetClinic Sample Application localy using Docker

- **Subtask I - Application**
    * Installing Docker: 
    * Set up the repository:
      * Update the apt package index and install packages to allow apt to use a repository over HTTPS:
       ```
       sudo apt-get update
       sudo apt-get install \
       apt-transport-https \
       ca-certificates \
       curl \
       gnupg \
       lsb-release
       ```
    * Add Docker’s official GPG key:
        ``` 
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings
        docker-archive-keyring.gpg 
        ```
    * Used the following command to set up the stable repo:
       ```
       $ echo \
        "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
       $ (lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null 
       ```
    * Installed docker engine
      ```
      $ sudo apt-get update
      $ sudo apt-get install docker-ce docker-ce-cli containerd.io
      ```
    * Look [app folder](https://gitlab.com/elshanfarzali/devopsdemo2/-/tree/master/app-folder) for `Dockerfile` to build petclinic-app image. 
- **Subtask II - Database** 
    * It runs Dockerfile in db-folder. For database container I used ansible-vault to encrypt my secure variables in [var.yml](https://gitlab.com/elshanfarzali/devopsdemo2/-/blob/master/variables/var.yml) and [other variable file (env.yml)](https://gitlab.com/elshanfarzali/devopsdemo2/-/blob/master/variables/env.yml) for non-secure just to define them in my playbook. Both of them are in variables folder. For using encrypted variables you must create your own to create container with correct way. You should set variables below:
      ```
      mysql_user:
      mysql_pass: 
      mysql_root_pass:
      mysql_database:
      ```
    * In order to create images and container manually you should run [main.yml](https://gitlab.com/elshanfarzali/devopsdemo2/-/blob/master/main.yml) file. In this file I defined two roles to build images and run containers. One of them is for DB, another is for APP. 
      Run command: `ansible-playbook main.yml --vault-password-file ${your-vault-password-file}` 
      For running this command you should create your own vault password file. If you do not, use `ansible-playbook main.yml --ask-vault-pass` command.
 
- **Subtask III - Docker registry**
    * I create docker registry in DockerHub. 
    * After login there with `docker login` command, in order to push image there we should tag images.
      ```
      docker tag {source_image}:{source_tag} {registry_url}/{target_image}:{target_image_tag}
      docker push {registry_url}/{target_image}:{target_image_tag}
      ```


- **Subtask IV - Jenkins**
    * Install Jenkins
      On Debian and Debian-based distributions like Ubuntu you can install Jenkins through apt. Use link below to install Jenkins:
      [installing Jenkins on Ubuntu](https://www.jenkins.io/doc/book/installing/linux/#debianubuntu)
    * In order to create jobs to build db and app images and push them to the repository I create [Jenkinsfile](https://gitlab.com/elshanfarzali/devopsdemo2/-/blob/master/Jenkinsfile).

- **Additional tasks:**
    * Create your own docker registry on host machine and Push images there:
      I created NEXUS registry on my own. You can also find yaml file in [Nexus folder](https://gitlab.com/elshanfarzali/devopsdemo2/-/tree/master/nexus). I also show it below:
      ```
      version: "2"
      services:
        nexus:
          image: sonatype/nexus3
          volumes:
              - "nexus-data:/nexus-data"
          ports:
              - "8081:8081"
              - "8083:8083"
              - "8084:8084"
              - "8085:8085"
      volumes:
        nexus-data: {}
        ```
    * Create a job that triggered on changes:
      I used Job DSL syntax to trigger my job:
      ```
      properties {
        pipelineTriggers {
            triggers {
              gitlab{
                triggerOnPush(true)
              }
            }
        }
      }
      ```
   * Provide you script to create job for pipeline:
      ```
      definition {
        cpsScm{
          scm{
           git {
              remote{
                name('origin')
               url("$git_url")
              }
             branch("$branch_name")
            }
            scriptPath("Jenkinsfile")
          }
        }
      }
      ```
    * Now I want to provide you full DSL script: 
    
      ```
      String git_url = 'https://gitlab.com/elshanfarzali/devopsdemo2'
      String job_path = 'myjenkinsjob'
      String branch_name = '*/master'
      folder(job_path) {
	      description 'DSL generated folder.'
      }
      pipelineJob("$job_path/pipeline_for_demo2"){
        definition {
          cpsScm{
            scm{
              git {
                remote{
                  name('origin')
                  url("$git_url")
                }
                branch("$branch_name")
              }
              scriptPath("Jenkinsfile")
            }
          }
        }
        properties {
          pipelineTriggers {
            triggers {
              gitlab{
                triggerOnPush(true)
              }
            }
          }
        }
      }
    * Additional python script for myself - this script tests app so if app isn't running then images are not configured correctly. Otherwise everything is OK, images are pushed to the registry. You can find script in [test-cont.py file](https://gitlab.com/elshanfarzali/devopsdemo2/-/blob/master/test-cont.py)

