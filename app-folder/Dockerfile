# the first stage of our build will use a maven 3.6.1 parent image 
FROM maven:3.6.1-jdk-8-alpine AS MAVEN_BUILD 
# now define working dir and arguments
ARG JAR_FILE=/build/target/*.jar
ARG BUILD_BRANCH=master
WORKDIR /build
# clone git repo and use the pom and src code for the container if you do run locally
#RUN apk add git \
#        && git clone --single-branch --branch $BUILD_BRANCH https://gitlab.com/elshanfarzali/devopsdemo2.git \
#        && mv demo2/* .
COPY maven/ .
# build all dependencies
#COPY demo2/src .
#COPY demo2/pom.xml .
RUN mvn dependency:go-offline

# package our application code 
RUN mvn clean package 

# the second stage of our build will use open jdk 8 on alpine 3.9 our final base image
FROM openjdk:8-jre-alpine3.9
#create non-root user for app
ARG USER=elshan
ARG JAR_FILE=/build/target/*.jar
ENV HOME /home/$USER

RUN apk add --update sudo

RUN adduser -D $USER \
        && echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER \
        && chmod 0440 /etc/sudoers.d/$USER

USER $USER
#define working dir
WORKDIR $HOME

RUN sudo chown -R $USER:$USER $HOME
# copy only the artifacts we need from the first stage and discard the rest 
COPY --from=MAVEN_BUILD ${JAR_FILE}   demo2.jar

# set the startup command to execute the jar 
CMD ["java", "-jar", "demo2.jar"]