import docker
import os
import requests
import time
import sys

#if you want to check container is in case started or stopped uncomment the code below
"""
DOCKER_CLIENT = docker.DockerClient(base_url='unix://var/run/docker.sock')
RUNNING = 'running'

def is_running(container_name):
    container = DOCKER_CLIENT.containers.get(container_name)

    container_state = container.attrs['State']

    container_is_running = container_state['Status'] == RUNNING

    return container_is_running

my_container_name = "petclinic-app"
print(is_running(my_container_name))
"""

def status_app():
    url = "http://localhost:8082/actuator/health"
    res = requests.get(url)
    contcode = res.status_code
    body = res.content
    if (body == b'{"status":"UP"}') and (contcode == 200):
        print(body)
        print(contcode)
        print("All is OK")
        return 0        
    else:
        return 1
if status_app():
    sys.exit(1)
else:
    sys.exit(0)
